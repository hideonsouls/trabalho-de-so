import threading
import gerenciador

total_compras = 0

class myThread(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.name = 'Cliente ' + str(name) 
	
	def run(self):
		global total_compras
		print("%s entrou na loja!" % self.name)
		# Verifica se há produtos em estoque
		while gerenciador.verificar():
			print("%s iniciou a compra" % self.name)
			# Efetua a compra
			gerenciador.comprar()
			total_compras += 1
			print("Compra realizada por %s" % self.name)

threads = []

n = int(input('Número de produtos no estoque: '))
gerenciador.fix(n)

n = int(input('Número de clientes: '))

# Criação das threads
for i in range(n):
	new_thread = myThread(i+1)
	threads.append(new_thread)
	new_thread.start()

for t in threads:
	t.join()

# Mostra ultimos dados
print(' ', end='\n\n')
print(gerenciador.estoque(), 'produtos restam no estoque')
print(total_compras, 'compras realizadas')
print(' ', end='\n\n')
