import threading
import random
import time

total_compras = 0
favoredThread = 0
wantToEnter = []
threads = []
completed = {}

class myThread(threading.Thread):
    
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = 'Cliente ' + str(name)
        self.num = name-1

    def run(self):
        global total_compras
        global total_produtos
        global favoredThread
        global wantToEnter
        global completed
        

        wantToEnter[self.num] = True
        print("%s entrou na loja!" % self.name)
        # Verifica se a thread já cumpriu seu propósito (zerar o estoque de compras)
        while completed[self.num] == False:
            # Enquato houver produtos em estoque:
            while total_produtos > 0:
                # Enquanto a thread não for a única com a intenção == True 
                while(wantToEnter.count(True) > 1):
                    # Caso a thread não seja a thread favorita para realizar a execução, aguarda até que ela seja.
                    if(self.num != favoredThread):
                        wantToEnter[self.num] = False
                        while (self.num != favoredThread):
                            time.sleep(0.1)
                        #Thread se tornou a favorita
                        wantToEnter[self.num] = True

                # CRITICAL SECTION
                # Caso haja produto em estoque, inicia o processo de compra
                if total_produtos > 0:
                    print("%s iniciou a compra" % self.name)
                    total_produtos -= 1
                    total_compras += 1
                    print("Compra realizada por %s" % self.name)
                    wantToEnter[self.num] = False                       # Declara falta de intenção em entrar na área crítica.
                    time.sleep(0.5)                                       # Aguarda um tempo, antes de verificar que sua intenção deve voltar a ser verdadeira
                    if total_produtos > 0: wantToEnter[self.num] = True     # Caso ainda haja produtos em estoque, intenção é verdadeira novamente
                
                if ( total_produtos <= 0 ):
                    completed[self.num] = True
                    wantToEnter[self.num] = False
                    print("Thread %s completou a execução"% self.num)
                # END OF CRITICAL SECTION
    

        
def chooseNext():        # Sorteia outra thread que ainda não tenha encerrado sua execução
    global completed
    tempList = []
    
    for k in completed.keys():
        if completed[k] == False:
            tempList.append(k)
    return random.choice(tempList)

def countCompleted():   # Conta quantas threads já terminaram sua execução
    global completed
    count = 0
    for k in completed.values():
        if(k == True) : count+=1

    return count 


total_produtos = int(input('Número de produtos no estoque: '))

n = int(input('Número de threads: '))
for i in range(n):
    new_thread = myThread(i+1)
    wantToEnter.append(False)
    completed[i] = False
    threads.append(new_thread)
    new_thread.start()

# Enquanto houver threads que ainda não se encerraram, 
# escolhe uma nova thread favorita dentre as que ainda estão em execução
while ( countCompleted() < n ): 
    favoredThread = chooseNext()
    time.sleep(0.2)

for t in threads:
    t.join()
    print("joined %s" % t.num)

print(' ', end='\n\n\n')
print(total_produtos, 'produtos restam no estoque')
print(total_compras, 'compras realizadas')
print("Completed threads: " + str(completed))
print(' ', end='\n\n\n')