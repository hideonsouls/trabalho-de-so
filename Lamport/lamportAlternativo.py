import time
import threading

total_compras = 0

class myThread(threading.Thread):
	def __init__(self, ID):
		threading.Thread.__init__(self)
		self.ID = ID
		self.name = 'Cliente ' + str(ID+1) 
		self.request_queue = []

	def run(self):
		global total_compras
		global total_produtos
		print("%s entrou na loja!" % self.name)
		self.replyRequests(time.clock())
		if total_produtos > 0:
			timestamp = time.clock()
			self.setRequest(timestamp)
			while not self.waitReply(timestamp): pass
			while not self.waitQueue(timestamp): pass
			if total_produtos > 0:
				print("%s iniciou a compra" % self.name)
				total_produtos -= 1
				total_compras += 1
				print("Compra realizada por %s" % self.name)
			self.releaseQueue()

		while True in thread_status:
			self.replyRequests(time.clock())

	def priorityQueue(self,pair):
		if self.request_queue != []:
			for i in range(len(self.request_queue)):
				if self.request_queue[i][1] == pair[1]: return
				if pair[0] < self.request_queue[i][0]:
					self.request_queue.insert(i,pair)
					return
		self.request_queue.append(pair)

	def replyRequests(self, timestamp):
		global reply_queue
		global request_set
		if self.request_queue != []:
			aux = self.request_queue[0][1]
			if not request_set[aux]: self.request_queue.pop(0)	
		for i in range(len(reply_queue)):
			if 	request_set[i]:
				self.priorityQueue([reply_queue[i][i], i])
				reply_queue[i][self.ID] = timestamp

	def setRequest(self, timestamp):
		global reply_queue
		global request_set
		request_set[self.ID] = True
		reply_queue[self.ID][self.ID] = timestamp
		self.priorityQueue([timestamp,self.ID])

	def waitReply(self, timestamp):
		global reply_queue	
		self.replyRequests(timestamp)
		for i in range(len(reply_queue)):
			if reply_queue[self.ID][self.ID] > reply_queue[self.ID][i]: return False		
		return True

	def waitQueue(self,timestamp):
		self.replyRequests(timestamp)
		if self.request_queue[0][1] == self.ID: return True
		return False

	def releaseQueue(self):
		global reply_queue
		global request_set
		global thread_status
		self.request_queue.pop(0)
		reply_queue[self.ID][self.ID] = -1
		request_set[self.ID] = False
		thread_status[self.ID] = False

total_produtos = int(input('Número de produtos no estoque: '))

n = int(input('Número de clientes: '))

reply_queue = [[-1]*n]*n
request_set = [False]*n
thread_status = [True]*n
threads = []

for i in range(n):
	new_thread = myThread(i)
	threads.append(new_thread)
	new_thread.start()

for t in threads:
	t.join()

print(' ', end='\n\n')
print(total_produtos, 'produtos restam no estoque')
print(total_compras, 'compras realizadas')
print(' ', end='\n\n')
