import threading

total_compras = 0

class myThread(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.name = 'Cliente ' + str(name) 
	
	def run(self):
		global total_compras
		global total_produtos
		print("%s entrou na loja!" % self.name)
		while total_produtos > 0:
			print("%s iniciou a compra" % self.name)
			total_produtos -= 1
			total_compras += 1
			print("Compra realizada por %s" % self.name)

threads = []

total_produtos = int(input('Número de produtos no estoque: '))
n = int(input('Número de clientes: '))

for i in range(n):
	new_thread = myThread(i+1)
	threads.append(new_thread)
	new_thread.start()

for t in threads:
	t.join()

print(' ', end='\n\n')
print(total_produtos, 'produtos restam no estoque')
print(total_compras, 'compras realizadas')
print(' ', end='\n\n')
