import gerenciador
import threading
import time

def inicializar(n):#cria os vetores nivel com n posições em -1 e ultimo_a_entrar com n-1 posições em -1
	global nivel
	global ultimo_a_entrar
	nivel = [-1] * n
	ultimo_a_entrar = [-1] * (n-1)

def verifica_nivel(name):  #retorna true se alguem tiver no mesmo nivel de espera ou em um nivel a frente
	global nivel
	for i in range(n):
		if(i != name):
			if(nivel[i] >= nivel[name]):
				return True
	return False

def enter_region (name):
	global nivel
	global ultimo_a_entrar
	for i in range(n-1):
		nivel[name] = i
		ultimo_a_entrar[i] = name
		while( (ultimo_a_entrar[i] == name) and verifica_nivel(name) ): 
			pass

def leave_region(name):
	nivel[name] = -1

class myThread(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.name = 'Cliente ' + str(name)
		self.id = int(name)

	def run(self): # metodo das threads que realiza a compra
		global total_compras
		print("%s entrou na loja!" % self.name)
		while  gerenciador.verificar():# esse acesso a região critica pode ser feito antes do enter region pois é apenas de leitura e a real verificação é feita pelo proximo if
			enter_region(self.id) #chamada da função enter_region()
			if gerenciador.verificar():
				print("%s iniciou a compra" % self.name)
				gerenciador.comprar()
				total_compras += 1
				print("Compra realizada por %s" % self.name)
			leave_region(self.id)#chamada da função leave_region


nivel = [] #o nivel da sala de espera da thread [i]
ultimo_a_entrar = []# ultimo processo a entrar na sala [i]

total_compras = 0

threads = []

n = int(input('Número de produtos no estoque: '))
gerenciador.fix(n)
n = int(input('Número de clientes: '))
inicializar(n)

for i in range(n):# inicia n threads
	new_thread = myThread(i)
	threads.append(new_thread)
	new_thread.start()

for t in threads:
	t.join()

print(' ', end='\n\n\n')
print(gerenciador.estoque(), 'produtos restam no estoque FINALIZADO \n')
print(total_compras, 'compras realizadas FINALIZADO\n')
print(' ', end='\n\n\n')
