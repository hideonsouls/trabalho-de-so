import threading
import time

def inicializar(n):#cria os vetores
	global nivel
	global ultimo_a_entrar
	nivel = [-1] * n
	ultimo_a_entrar = [-1] * (n-1)

def verifica_nivel(name):  #retorna true se alguem tiver na mesma sala ou frente
	global nivel
	for i in range(n):
		if(i != name):
			if(nivel[i] >= nivel[name]):
				return True
	return False

def enter_region (name):
	global nivel
	global ultimo_a_entrar
	for i in range(n-1):
		nivel[name] = i
		ultimo_a_entrar[i] = name
		while( (ultimo_a_entrar[i] == name) and verifica_nivel(name) ): 
			pass

def leave_region(name):
	nivel[name] = -1

class myThread(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.name = 'Cliente ' + str(name)
		self.id = int(name)

	def run(self):
		global total_compras
		global total_produtos
		print("%s entrou na loja!" % self.name)
		while  total_produtos > 0:
			enter_region(self.id)
			if total_produtos > 0:
				print("%s iniciou a compra" % self.name)
				total_produtos -= 1
				total_compras += 1
				print("Compra realizada por %s" % self.name)
			leave_region(self.id)


nivel = [] #o nivel da sala de espera da thread [i]
ultimo_a_entrar = []# ultimo processo a entrar na sala [i]

total_compras = 0

threads = []

total_produtos = int(input('Número de produtos no estoque: '))
n = int(input('Número de clientes: '))
inicializar(n)

for i in range(n):
	new_thread = myThread(i)
	threads.append(new_thread)
	new_thread.start()

for t in threads:
	t.join()

print(' ', end='\n\n\n')
print(total_produtos, 'produtos restam no estoque FINALIZADO \n')
print(total_compras, 'compras realizadas FINALIZADO\n')
print(' ', end='\n\n\n')
