import pymysql

def comprar():
	connection = pymysql.connect(
		host = 'localhost',
		user = 'root',
		password = 'abacateatomico',
		db = 'estoque'
	)
	with connection.cursor() as cursor:
		cursor.execute(" UPDATE lojateste SET quantidade = quantidade-1 WHERE nome = 'Teste' ")
		connection.commit()	

def verificar():
	connection = pymysql.connect(
		host = 'localhost',
		user = 'root',
		password = 'abacateatomico',
		db = 'estoque'
	)
	with connection.cursor() as cursor:
		cursor.execute(" SELECT quantidade FROM lojateste WHERE nome = 'Teste' ")
		result = cursor.fetchone()
		return result[0] > 0

def estoque():
	connection = pymysql.connect(
		host = 'localhost',
		user = 'root',
		password = 'abacateatomico',
		db = 'estoque'
	)
	with connection.cursor() as cursor:
		cursor.execute(" SELECT quantidade FROM lojateste WHERE nome = 'Teste' ")
		result = cursor.fetchone()
		return result[0]

def fix(n = 15):
	connection = pymysql.connect(
		host = 'localhost',
		user = 'root',
		password = 'abacateatomico',
		db = 'estoque'
	)
	with connection.cursor() as cursor:
		cursor.execute(" UPDATE lojateste SET quantidade = %s WHERE nome = 'Teste' " % n)
		connection.commit()
